VERSION_NAME?=$(shell git branch --show-current | sed -r 's/\//-/g')
VERSION_NAME_SHA?=$(shell /bin/echo -n "$(VERSION_NAME)" | openssl sha1 -r)
NAMESPACE_POSTFIX?=-$(shell a="$(VERSION_NAME_SHA)"; /bin/echo "$${a:0:6}")
FAAS_GATEWAY?=

CLUSTER_NAME?=
LOCATION?=
KUBE_DIR?=$(HOME)/.kube

namespace=$(NAMESPACE)$(NAMESPACE_POSTFIX)
function_namespace=$(namespace)-fn

ifeq ($(IMAGE_BASE),)
FN_IMAGE_BASE ?= 
else
FN_IMAGE_BASE ?= $(IMAGE_BASE)/functions/
endif

define faas-cli
NAMESPACE=$(namespace) \
VERSION=$(VERSION) \
VERSION_NAME=$(VERSION_NAME) \
CLUSTER_NAME=$(CLUSTER_NAME) \
LOCATION=$(LOCATION) \
GATEWAY=$(FAAS_GATEWAY) \
IMAGE_BASE=$(FN_IMAGE_BASE) \
faas-cli $(1) --tls-no-verify -n $(function_namespace)
endef

define fetch-password
kubectl \
	--context $(CLUSTER_NAME)-$(LOCATION) \
	--kubeconfig $(KUBE_DIR)/$(CLUSTER_NAME) \
	-n faas get secrets basic-auth -o=jsonpath='{.data.basic-auth-password}' | base64 -d
endef

faas-cli-login:
	@$(fetch-password) | faas-cli login --password-stdin -u admin -g $(FAAS_GATEWAY) --tls-no-verify  --timeout 30s

faas-cli:
	@cd functions && $(call faas-cli,$(cmd))

.PHONY: faas-up
faas-up:
	@cd functions; \
		$(foreach func,\
			$(wildcard functions/*.yml), \
			$(call faas-cli,up -f $(notdir $(func)),--tls-no-verify -n $(function_namespace));\
		)

.PHONY: faas-build
faas-build:
	@cd functions; \
		$(foreach func,\
			$(wildcard functions/*.yml), \
			$(call faas-cli,build -f $(notdir $(func)));\
		)

.PHONY: faas-push
faas-push:
	@cd functions; \
		$(foreach func,\
			$(wildcard functions/*.yml), \
			$(call faas-cli,push -f $(notdir $(func)));\
		)

.PHONY: faas-deploy
faas-deploy:
	@cd functions; \
		$(foreach func,\
			$(wildcard functions/*.yml), \
			$(call faas-cli,deploy -f $(notdir $(func)),--tls-no-verify -n $(function_namespace));\
		)

.PHONY: faas-remove
faas-remove:
	@cd functions && \
		$(foreach func,\
			$(wildcard functions/*.yml), \
			$(call faas-cli,remove -f $(notdir $(func)),--tls-no-verify -n $(function_namespace));\
		)
