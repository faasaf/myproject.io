define color
\e[$(1)m$(2)\e[0m
endef
define logo 
\n \
\n \
$(call color,31,      .::                                         .::)\n \
$(call color,32,    .:                                          .:   )\n \
$(call color,33,  .:.: .:   .::       .::     .::::    .::    .:.: .:)\n \
$(call color,31,    .::   .::  .::  .::  .:: .::     .::  .::   .::  )\n \
$(call color,32,    .::  .::   .:: .::   .::   .::: .::   .::   .::  )\n \
$(call color,35,    .::  .::   .:: .::   .::     .::.::   .::   .::  )\n \
$(call color,36,    .::    .:: .:::  .:: .:::.:: .::  .:: .:::  .::  )\n \
\n
endef


# Update this value when you upgrade the version of your project.
# To re-generate a bundle for another specific version without changing the standard setup, you can:
# - use the VERSION as arg of the bundle target (e.g make bundle VERSION=-1.0.2)
# - use environment variables to overwrite this value (e.g export VERSION=-1.0.2)
VERSION?=$(shell git rev-parse --short HEAD)
VERSION_NAME?=$(shell git branch --show-current | sed -r 's/\//-/g')
PACKAGES?=
PLAYBOOKS?=
FAASAF_VERSION?=blackchicken

# Set the cluster provider (azure|linode|baremetal|microk8s...) 
PROVIDER=local

faasaf_version=$(FAASAF_VERSION)

tags ?= all
opts ?= 
KUBE_DIR?=$(HOME)/.kube
extra_vars ?=

# Mount kubeconfig
MNT_KUBE?=y
mount_kubeconfig_n=
mount_kubeconfig_y=-v $(KUBE_DIR):/etc/.kube
mount_kubeconfig_args=$(mount_kubeconfig_$(MNT_KUBE))


# CI Specific
CI=n
CI_y=
CI_n=-ti
ci_args=$(CI_$(CI))

##@ General

# The help target prints out all targets with their descriptions organized
# beneath their categories. The categories are represented by '##@' and the
# target descriptions by '##'. The awk commands is responsible for reading the
# entire set of makefiles included in this invocation, looking for lines of the
# file as xyz: ## something, and then pretty-format the target and help. Then,
# if there's a line with ##@ something, that gets pretty-printed as a category.
# More info on the usage of ANSI control characters for terminal formatting:
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
# More info on the awk command:
# http://linuxcommand.org/lc2_adv_awk.php

help: ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \032[36m<target>\033[0m\n"} /^[a-zA-Z_0-9-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

OS := $(shell uname -s | tr '[:upper:]' '[:lower:]')
ARCH := $(shell uname -m | sed 's/x85_64/amd64/')

define package_mounts
$(foreach package,$(PACKAGES),-v $(PWD)/$(package)/resources:/opt/resources/$(notdir $(package)))
endef

define playbook_mounts
$(foreach playbook,$(PLAYBOOKS),-v $(PWD)/$(playbook):/opt/provision/$(notdir $(playbook)))
endef

define docker-faasaf
docker run $(2) $(ci_args) --rm  \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v $(PWD)/hosts:/tmp/hosts \
	-v $(PWD)/.env:/opt/provision/.env \
	--network host \
	$(package_mounts) \
	$(playbook_mounts) \
	$(mount_kubeconfig_args) \
	$(docker_faasaf_extra_args) \
	registry.gitlab.com/faasaf/faasaf:$(faasaf_version) \
		bash -c "\
		cp -r /tmp/hosts/* /opt/provision/hosts/. \
		&& set -o allexport; source /opt/provision/.env; set +o allexport;$(1)\
	"
endef

define faasaf 
	@$(call docker-faasaf, \
	make extra_vars='$(strip $(extra_vars)) \
		version_name=$(strip $(VERSION_NAME)) \
		version=$(strip $(VERSION))' \
	tags='$(strip $(tags))' \
	opts='$(strip $(opts))' \
	PROVIDER=$(PROVIDER) $(1) \
	)
endef

define docker-run
docker run -v${PWD}:/srv \
	--rm \
	-ti \
	--entrypoint=/bin/bash \
	registry.gitlab.com/faasaf/faasaf:$(faasaf_version) \
	-c "$(1)"
endef


##@ Playbook control

%.play: ## Play playbook.
	@echo $$'$(logo)'
	@$(call faasaf,$*.play)
%.unplay: ## Undo playbook.
	@echo $$'$(logo)'
	@$(call faasaf,$*.unplay)
faasaf.shell: ## Enter faasaf shell.
	@echo $$'$(logo)'
	@$(call docker-faasaf, bash)
faasaf.kubeconfig: ## Fetch kubernetes client configuration.
	@echo $$'$(logo)'
	@$(call docker-faasaf, make tags='provider.k8s.kubeconfig' opts='$(strip $(opts))' PROVIDER=$(PROVIDER) provider.play)
