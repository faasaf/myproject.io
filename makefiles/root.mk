include ./makefiles/faasaf.mk

VERSION?=$(shell git rev-parse --short HEAD)
VERSION_NAME?=$(shell git branch --show-current | sed -r 's/\//-/g')
IMAGE_BASE?=
TAG?=
DEV?=n
PACKAGES?=
PLAYBOOKS?=
hostpath?=$(PWD)

ifndef extra_vars
define extra_vars
hostpath=$(hostpath)
endef
endif

define make-apps
for i in $(PACKAGES); do \
	cd $${i} && make TAG=$(TAG) DEV=$(DEV) VERSION=$(VERSION) VERSION_NAME=$(VERSION_NAME) IMAGE_BASE=$(IMAGE_BASE) $(1); \
done
endef

define docker-run
docker run -v${PWD}:/srv \
	--rm \
	-ti \
	--entrypoint=/bin/bash \
	registry.gitlab.com/faasaf/faasaf:$(FAASAF_VERSION) \
	-c "$(1)"
endef

.PHONY: docker-build
docker-build:
	@$(call make-apps, docker-build)

.PHONY: docker-tag
docker-tag:
	@$(call make-apps, docker-tag)

.PHONY: docker-push
docker-push:
	@$(call make-apps, docker-push)

.PHONY: functions-up
functions-up:
	@$(call make-apps, faas-cli-login faas-up)

sample:
	@$(call docker-run, cp -r _sample /srv/sample)

hosts:
	@$(call docker-run, cp -r hosts /srv/)

.PHONY: scaffold
scaffold: hosts sample
