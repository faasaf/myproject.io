VERSION?=$(shell git rev-parse --short HEAD)
VERSION_NAME?=$(shell git branch --show-current | sed -r 's/\//-/g')
IMAGE_BASE ?= 
IMAGE_NAME ?=
TAG ?=
DEV=n

dockerfile_dev_n=Dockerfile
dockerfile_dev_y=Dockerfile.dev

dockerfile=$(dockerfile_dev_$(DEV))

ifeq ($(IMAGE_BASE),)
image ?= $(IMAGE_NAME)
else
image ?= $(IMAGE_BASE)/$(IMAGE_NAME)
endif

build_args ?=

ifeq ($(TAG),)
define push
	docker push $(image):$(VERSION)
endef
else
define push
	docker push $(image):$(VERSION) && \
	docker push $(image):$(TAG)
endef
endif

prebuild-dev-y?=
prebuild-dev-n?=

prebuild?=$(prebuild-dev-$(DEV))

.PHONY: docker-build
docker-build:
	@$(prebuild)
	@docker build -f $(dockerfile) $(build_args) -t $(image):$(VERSION) .

.PHONY: docker-tag
docker-tag:
	@docker tag $(image):$(VERSION) $(image):$(TAG)

.PHONY: docker-push
docker-push:
	@$(push)
