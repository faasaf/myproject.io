"use strict";
module.exports = async (event, context) => {
  const result = {
    body: JSON.stringify(event.body.toEcho),
    contentType: "application/json",
  };

  return context
    .headers({
      "content-type": "application/json",
    })
    .status(200)
    .succeed(result);
};
