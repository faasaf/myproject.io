import { createApp, h, provide } from "vue";
import { createPinia } from "pinia";
import { ApolloClient, InMemoryCache } from "@apollo/client/core";
import App from "./App.vue";
import router from "./router";
import "./assets/main.css";
import { DefaultApolloClient } from "@vue/apollo-composable";
import { plugin, defaultConfig } from '@formkit/vue'

const cache = new InMemoryCache();

const apolloClient = new ApolloClient({
  cache,
  uri: `https://${import.meta.env.VITE_DOMAIN}/graphql`,
});

const app = createApp({
  setup() {
    provide(DefaultApolloClient, apolloClient);
  },
  render: () => h(App),
});

app.use(createPinia());
app.use(router);
app.use(plugin, defaultConfig);

app.mount("#app");
