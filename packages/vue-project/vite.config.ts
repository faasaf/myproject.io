import { fileURLToPath, URL } from "node:url";

import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default (mode: any) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };

  const server = !!process.env.VITE_USE_HTTPS
    ? {
        host: "0.0.0.0",
        hmr: {
          clientPort: 443,
        },
      }
    : {};

  return defineConfig({
    server,
    plugins: [vue()],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
  });
};
